﻿using System;

namespace ExampleDungeon
{
	public struct CryptaRoom
	{
		static public bool m_opendGrave = false;

		static public void EnterRoom ()
		{
			Console.Clear ();
			Console.WriteLine ("You entered the crypta !\n\n");
			Console.WriteLine ("You can hear some weird voices in the room...\n" +
				"(1) Go take a look at the big door.\n");

			if (!m_opendGrave) {
				Console.WriteLine ("(2) Open the mysterious grave.\n");
			}
			ConsoleKey pressedKey = Console.ReadKey ().Key;
			switch (pressedKey) {
			case(ConsoleKey.D1):
				{
					if (ExampleDungeon.m_heroProfession == "Ninja") {

						Console.WriteLine ("\nThis door shows some kind of ancient family....Looks like they where a family of ninjas.\n");
						Console.WriteLine ("\nYou are able to open the door.\n");
						Console.ReadKey ();
						Entrance_Room.EnterRoom ();

					} else {

						Console.WriteLine ("\nThis door shows some kind of ancient family....Looks like they where a family of ninjas.\n");
						Console.WriteLine ("\nStrangely, you can not open it..\n");
						Console.ReadKey ();
						EnterRoom ();
					}
					break;
				}
			case(ConsoleKey.D2):
				{
					if (!m_opendGrave) {

						Console.WriteLine ("\nWhen you open the grave, you see some souls leaving it, crying and shouting some strange sentences.\n");
						m_opendGrave = true;
						Console.ReadKey ();
						EnterRoom ();

					} else {

						InvalidInput ();
						EnterRoom ();
					}
					break;
				}
			default:
				{
					InvalidInput ();
					break;

						
				}
		
}
}

		static void InvalidInput()
		{
			Console.Clear ();
			Console.WriteLine ("Invalid Choice - Please try again \n\n");
			Console.ReadKey ();
			EnterRoom ();
		}
	}
}


