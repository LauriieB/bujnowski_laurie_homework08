﻿using System;

namespace ExampleDungeon
{
	class ExampleDungeon
	{
		public static string m_heroProfession;
		public static void Main (string [] args)
		{
			Console.Title = "EXAMPLE DUNGEON";
			Console.BackgroundColor = ConsoleColor.DarkRed;
			Console.ForegroundColor = ConsoleColor.White;
			Console.Clear ();

			Console.WriteLine ("Welcome hero, it's now time to test your strenght and courage");
			Choice_HeroProfession ();

			Console.WriteLine ("\nThe unnamed "+ m_heroProfession +" is entering the cave\n" + 
				"The entrance collapses. Now, you have to find the exit.\n");

			Console.ReadKey();
			Entrance_Room.EnterRoom ();
			//Ici, o appelle la fonction pour changer de pièce

		}
		static void Choice_HeroProfession()
		{
			Console.WriteLine ("Adventurer....what is your calling?\n\n"+
				"(1) I am a professional assasin!\n" +
				"(2) I am a shadow ninja!\n" +
				"(3) I am a tactical warrior!\n");

			ConsoleKey pressedKey = Console.ReadKey().Key;

			switch(pressedKey) {
			case(ConsoleKey.D1):
				{
					m_heroProfession = "Assassin";

				}
				break;
			case(ConsoleKey.D2):
				{
					m_heroProfession = "Ninja";

				}
				break;
			case(ConsoleKey.D3):
				{
					m_heroProfession = "Warrior";

				}
				break;
				default:
				{
					Console.Clear();
					Console.WriteLine ("I think you didn't understand my question....What are you?!\n\n");
					Choice_HeroProfession();
					break;
				}


			}

	
		}
	}
}
